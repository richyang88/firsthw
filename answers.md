### Directions

Refer back to the notes from today and/or use the internet to find the answers to the questions below:

#### Answer the following questions

1. What command do you use to setup a git repository inside of your folder?
Answer: git init

2. What command do you use to ask git to start tracking a file?
Answer: git add

3. What command do you use to ask git to move your file from the staging area to the repository?<br>
Answer: git commit -m

4. What command do you use to get updates from the class repository?
Answer: git pull

5. What command do you use to push your work to your fork of the class repository?
Answer: git push
 
1. What command do you use to unstage a file?<br>
Answer: git reset

1. What command do you use to change your files back to how they were after a commit?<br>
Answer: git undo

1. Why is it important to use `--` when chaging files back to a previous state?<br>
Answer: to make terminal read as file path instead of code

1. Why might you want to reset your files back to a previous commit?<br>
Answer: Due to bugs, errors, change of decision to content. 
